console.log('Happywalks.js is loaded!');

document.addEventListener('DOMContentLoaded', function () {

  // Get all "navbar-burger" elements
  var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

  // Check if there are any navbar burgers
  if ($navbarBurgers.length > 0) {

    // Add a click event on each of them
    $navbarBurgers.forEach(function ($el) {
      $el.addEventListener('click', function () {

        // Get the target from the "data-target" attribute
        var target = $el.dataset.target;
        var $target = document.getElementById(target);

        // Toggle the class on both the "navbar-burger" and the "navbar-menu"
        $el.classList.toggle('is-active');
        $target.classList.toggle('is-active');

      });
    });
  }

});



function onClickTag(e) {
    //console.log('Tag Cliqué');
    //console.log(e.innerHTML);
    //console.log(e);
    if (e.className.search('is-primary') != -1) {
        e.className = 'tag is-medium pointer';
    } else {
        e.className = 'tag is-medium pointer is-primary';
    }
}
function onClickTag2(e) {
    //console.log('Tag Cliqué');
    //console.log(e.innerHTML);
    //console.log(e);
    if (e.className.search('btn-info') != -1) {
        e.className = 'btn btn-light btn-sm';
    } else {
        e.className = 'btn btn-info btn-sm';
    }
}


function onClickVisite(e) {
    //console.log('Visite Cliqué');
    //console.log(e.innerHTML);
    console.log(e);
    if (e.className.search('is-light')) {
        e.className = 'notification is-info';
    } else if (e.className.search('is-info')) {
        e.className = 'notification is-light';
    }
}

function etoile(e) {
    //console.log('étoile hover');
}