<!doctype html>
<html lang="fr">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <title>Happywalks.fr ~ accueil</title>
        <!--
        <link rel="stylesheet" href="css/knacss-unminified.css">-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css">
        <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
        <link rel="icon" type="image/png" href="img/logo-hw-2-yellow-crop.png" />
        <script src="js/happywalks.js"></script>
        <link rel="stylesheet" href="css/happywalks.css">
    </head>
    <body class="ubuntu">
        <header>
            <nav class="navbar is-transparent">
                <div class="navbar-brand">
                    <a class="navbar-item" href="">
                        <img src="img/logo-hw-2-yellow-crop.png" alt="Happywalks: application de parcours personalisés" height="28">
                    </a>
                    <div class="navbar-burger burger" data-target="navbarExampleTransparentExample">
                    <span>Home</span>
                    <span>Parcours</span>
                    <span>Préférences</span>
                    <span>Application</span>
                    </div>
                </div>

                <div id="navbarExampleTransparentExample" class="navbar-menu">
                    <div class="navbar-start">
                    <a class="navbar-item" href="">
                        Accueil
                    </a>
                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link" href="#parcours">
                        Parcours
                        </a>
                        <div class="navbar-dropdown is-boxed">
                        <a class="navbar-item" href="#parcours">
                            Tous
                        </a>
                        <hr class="navbar-divider">
                        <a class="navbar-item" href="#parcour_all_in_one_day">
                            All in One Day
                        </a>
                        <a class="navbar-item" href="#parcour_vol_oiseau">
                            A vol d'oiseau
                        </a>
                        <a class="navbar-item" href="#parcour_formule_1">
                            Formule 1
                        </a>
                        </div>
                    </div>
                    <a class="navbar-item" href="#preferences">
                        Préférences
                    </a>
                    <a class="navbar-item" href="#application">
                        Application
                    </a>
                    </div>

                    <div class="navbar-end">
                    <div class="navbar-item">
                        <div class="field is-grouped">
                        <p class="control">
                            <a class="bd-tw-button button" data-social-network="Twitter" data-social-action="tweet" data-social-target="http://localhost:4000" target="_blank" href="https://twitter.com/intent/tweet?text=Bulma: a modern CSS framework based on Flexbox&amp;hashtags=bulmaio&amp;url=http://localhost:4000&amp;via=jgthms">
                            <span class="icon">
                                <i class="fab fa-twitter"></i>
                            </span>
                            <span>
                                Tweet
                            </span>
                            </a>
                        </p>
                        <p class="control">
                            <a class="button is-link" href="https://github.com/jgthms/bulma/archive/0.5.1.zip">
                            <span class="icon">
                                <i class="fas fa-download"></i>
                            </span>
                            <span>Download</span>
                            </a>
                        </p>
                        </div>
                    </div>
                    </div>
                </div>
            </nav>
            <br /><br /><br /><br />
            <div style="text-align:center;margin-top:5vw;">
                <a href="">
                    <img src="img/logo-hw-2-yellow-crop.png" alt="happywalks-logo" title="Enjoy you trip !" width="120px" /><br />
                </a>
            </div>
            <div style="text-align:center;margin-top:1vw;">
                <h1 class="title">Happy Walks</h1>
                <h2 class="subtitle">Votre parcours commence ici !</h2>
            </div>
        </header>
        <main>
            <div class="ubuntu" style="width:9vw;margin:auto;margin-top:5vw;">           
                <ul style="list-style-type:none;padding-left: 0;">
                    <li><a class="button is-light" href="#parcours">Parcours</a></li>
                    <li><a class="button is-light" href="#preferences">Préférences</a></li>
                    <li><a class="button is-light" href="#application">Application</a></li>
                </ul>
            </div>
            <!-- <br /><br /><br />
            <div class="container">
                <a class="button is-light" href="#parcours">Parcours</a> 
                <a class="button is-light" href="#preferences">Préférences</a> 
                <a class="button is-light" href="#application">Application</a>
            </div>
            <br /><br /><br /> -->
            <br /><br /><br /><br />
            <a id="parcours"></a>
            <section class="hero is-primary">
                <div class="hero-body">
                    <div class="container">
                    <h1 class="title">
                        Parcours
                    </h1>
                    <h2 class="subtitle">
                        Catalogue
                    </h2>
                    </div>
                </div>
            </section>
            
            <a id="parcour_all_in_one_day"></a>
            <div class="box">
                <article class="media">
                    <div class="media-left">
                    <figure class="image is-128x128">
                        <img src="img/parcours/all_in_one_day/all_in_one_day1.jpg" alt="Image">
                    </figure>
                    </div>
                    <div class="media-content">
                    <div class="content">
                        <p>
                        <strong>All in one day</strong> <small>#All_In_One_Day</small>
                        <br>
                        Faites le tour de Monaco, ne manquez aucun coin bucolique, découvrez les grands endroits qui font la renommée de la ville ainsi que les ruelles et les marchés célèbres. Tout en un jour va satisfaire votre soif de découvrir l'âme de la ville en vous faisant passer par plusieurs quartiers.
                        </p>
                    </div>
                    <nav class="level is-mobile">
                        <div class="level-left">
                        <a class="level-item">
                            <span class="icon is-small"><i class="fas fa-retweet"></i></span>
                        </a>
                        <a class="level-item">
                            <span class="icon is-small"><i class="fas fa-heart"></i></span>
                        </a>
                        </div>
                    </nav>
                    </div>
                </article>
            </div>
            
            <a id="parcour_vol_oiseau"></a>
            <div class="box">
                <article class="media">
                    <div class="media-left">
                    <figure class="image is-128x128">
                        <img src="img/parcours/a_vue_doiseau/jardin1.jpg" alt="Image">
                    </figure>
                    </div>
                    <div class="media-content">
                    <div class="content">
                        <p>
                        <strong>A vue d'oiseau</strong> <small>#Jardins_Monaco</small>
                        <br>
                        Découvrez le Monaco vert à travers les jardins, la roseraie et certains monuments incontournables. nous vous proposons d'aller sur les hauteurs pour admirer la vue imprenable depuis le jardin exotique, puis de redescendre au niveau du port.
                        </p>
                    </div>
                    <nav class="level is-mobile">
                        <div class="level-left">
                        <a class="level-item">
                            <span class="icon is-small"><i class="fas fa-retweet"></i></span>
                        </a>
                        <a class="level-item">
                            <span class="icon is-small"><i class="fas fa-heart"></i></span>
                        </a>
                        </div>
                    </nav>
                    </div>
                </article>
            </div>
            
            <a id="parcour_formule_1"></a>
            <div class="box">
                <article class="media">
                    <div class="media-left">
                    <figure class="image is-128x128">
                        <img src="img/parcours/formule1/formule1.jpg" alt="Image">
                    </figure>
                    </div>
                    <div class="media-content">
                    <div class="content">
                        <p>
                        <strong>Formule 1</strong> <small>#Grand_Prix_de_Monaco_2018</small>
                        <br>
                        Le Grand Prix de Monaco est une des plus anciennes et l'une des trois courses les plus prestigieuses au monde, disputée en Principauté de Monaco, sur un circuit urbain. Nous vous proposons sa version interactive et personalisée !
                        </p>
                    </div>
                    <nav class="level is-mobile">
                        <div class="level-left">
                        <a class="level-item">
                            <span class="icon is-small"><i class="fas fa-retweet"></i></span>
                        </a>
                        <a class="level-item">
                            <span class="icon is-small"><i class="fas fa-heart"></i></span>
                        </a>
                        </div>
                    </nav>
                    </div>
                </article>
            </div>

            <br /><br /><br /><br /><br /><br />
            
            <a id="preferences"></a>
            <section class="hero is-info">
                <div class="hero-body">
                    <div class="container">
                    <h1 class="title">
                        Préférences
                    </h1>
                    <h2 class="subtitle">
                        Personalisation
                    </h2>
                    </div>
                </div>
            </section>

            <section class="hero">
                <div class="hero-body">
                    <div class="container">
                    <h1 class="title">
                        Je suis
                    </h1>
                    <h2 class="subtitle">
                        choisissez
                    </h2>
                    </div>
                </div>
            </section>
            <div class="container">
                <div class="notification">
                    <div class="control has-text-centered">
                        <label class="radio">
                            <img src="img/icones/noun_613975_cc.png" width="60px"/><br />
                            <input type="radio" name="type_visiteur0" checked>
                            homme
                        </label>
                        <label class="radio">
                            <img src="img/icones/noun_613938_cc.png" width="60px"/><br />
                            <input type="radio" name="type_visiteur0" >
                            femme
                        </label>
                    </div>
                </div>
                <div class="notification">
                    <div class="control has-text-centered">
                        <label class="radio">
                            <img src="img/icones/noun_395108_cc.png" width="80px"/><br />
                            <input type="radio" name="type_visiteur1" checked>
                            nouveau visiteur
                        </label>
                        <label class="radio">
                            <img src="img/icones/noun_632074_cc.png" width="65px"/><br />
                            <input type="radio" name="type_visiteur1">
                            habitué(e)
                        </label>
                    </div>
                </div>
                <div class="notification">
                    <div class="control has-text-centered">
                        <label class="radio">
                            <img src="img/icones/noun_1106963_cc.png" width="65px"/><br />
                            <input type="radio" name="type_visiteur2" checked>
                            famille
                        </label>
                        <label class="radio">
                            <img src="img/icones/noun_349141_cc.png" width="60px"/><br />
                            <input type="radio" name="type_visiteur2">
                            business
                        </label>
                        <label class="radio">
                            <img src="img/icones/noun_1106956_cc.png" width="65px"/><br />
                            <input type="radio" name="type_visiteur2">
                            couple
                        </label>
                        <label class="radio">
                            <img src="img/icones/noun_1106953_cc.png" width="63px"/><br />
                            <input type="radio" name="type_visiteur2">
                            seul(e)
                        </label>
                        <label class="radio">
                            <img src="img/icones/noun_1106972_cc.png" width="62px"/><br />
                            <input type="radio" name="type_visiteur2">
                            retraité(e)
                        </label>
                    </div>
                </div>
                <div class="notification">
                    <div class="control has-text-centered">
                        <label class="checkbox">
                            <img src="img/icones/noun_1106955_cc.png" width="45px"/><br />
                            <input type="checkbox">
                            animaux
                        </label>
                        <label class="checkbox">
                            <img src="img/icones/noun_1106982_cc.png" width="62px"/><br />
                            <input type="checkbox">
                            enceint
                        </label>
                        <label class="checkbox">
                            <img src="img/icones/noun_1050600_cc.png" width="60px"/><br />
                            <input type="checkbox">
                            handicapé(e)
                        </label>
                        <label class="checkbox">
                            <img src="img/icones/noun_640988_cc.png" width="60px"/><br />
                            <input type="checkbox">
                            équipé(e)
                        </label>
                    </div>
                </div>
            </div>

            <section class="hero">
                <div class="hero-body">
                    <div class="container">
                    <h1 class="title">
                        J'aime
                    </h1>
                    <h2 class="subtitle">
                        choisissez
                    </h2>
                    </div>
                </div>
            </section>
            <div class="container">
                <div class="notification">
                    <div class="tags pointer-hand select-disabled">
                        <span class="tag is-primary is-medium" onclick="onClickTag(this)">Nature</span>
                        <span class="tag is-primary is-medium" onclick="onClickTag(this)">Océan</span>
                        <span class="tag is-medium" onclick="onClickTag(this)">Casinos</span>
                        <span class="tag is-medium" onclick="onClickTag(this)">Sport</span>
                        <span class="tag is-medium" onclick="onClickTag(this)">Formule 1</span>
                        <span class="tag is-medium" onclick="onClickTag(this)">Voitures</span>
                        <span class="tag is-medium" onclick="onClickTag(this)">Gastronomie</span>
                        <span class="tag is-primary is-medium" onclick="onClickTag(this)">Architecture</span>
                        <span class="tag is-medium" onclick="onClickTag(this)">Histoire</span>
                        <span class="tag is-primary is-medium" onclick="onClickTag(this)">Plantes</span>
                        <span class="tag is-primary is-medium" onclick="onClickTag(this)">Marcher</span>
                        <span class="tag is-medium" onclick="onClickTag(this)">Musées</span>
                        <span class="tag is-medium" onclick="onClickTag(this)">Animaux</span>
                        <span class="tag is-medium" onclick="onClickTag(this)">Bâteaux</span>
                        <span class="tag is-medium" onclick="onClickTag(this)">Peinture</span>
                        <span class="tag is-primary is-medium" onclick="onClickTag(this)">Musique</span>
                        <span class="tag is-medium" onclick="onClickTag(this)">Techologies</span>
                        <span class="tag is-medium" onclick="onClickTag(this)">Jeux-Vidéos</span>
                        <span class="tag is-medium" onclick="onClickTag(this)">Produits Locaux</span>
                        <span class="tag is-medium" onclick="onClickTag(this)">Timbres</span>
                    </div>
                    <!--<div class="buttons">
                        <span class="button is-dark">Nature</span>
                        <span class="button is-dark">Océan</span>
                        <span class="button is-dark is-outlined">Casinos</span>
                        <span class="button is-dark is-outlined">Sport</span>
                        <span class="button is-dark is-outlined">Formule 1</span>
                        <span class="button is-dark is-outlined">Voitures</span>
                        <span class="button is-dark is-outlined">Gastronomie</span>
                        <span class="button is-dark">Architecture</span>
                        <span class="button is-dark is-outlined">Histoire</span>
                        <span class="button is-dark is-outlined">Plantes</span>
                        <span class="button is-dark">Marcher</span>
                        <span class="button is-dark is-outlined">Musées</span>
                        <span class="button is-dark">Animaux</span>
                        <span class="button is-dark is-outlined">Bâteaux</span>
                        <span class="button is-dark is-outlined">Peinture</span>
                        <span class="button is-dark">Musique</span>
                        <span class="button is-dark is-outlined">Techologies</span>
                        <span class="button is-dark is-outlined">Jeux-Vidéos</span>
                        <span class="button is-dark is-outlined">Produits Locaux</span>
                    </div>-->
                </div>
            </div>
            <section class="hero">
                <div class="hero-body">
                    <div class="container">
                    <h1 class="title">
                        J'ai le temps
                    </h1>
                    <h2 class="subtitle">
                        choisissez
                    </h2>
                    </div>
                </div>
            </section>
            <div class="container">
                <div class="notification">
                    <div class="control has-text-centered">
                        <label class="radio">
                            <img src="img/icones/noun_1428994_cc.png" width="50px"/><br />
                            <input type="radio" name="temps" checked>
                            pressé
                        </label>
                        <label class="radio">
                            <img src="img/icones/noun_709031.png" width="58px"/><br />
                            <input type="radio" name="temps">
                            modéré
                        </label>
                        <label class="radio">
                            <img src="img/icones/noun_181032_cc.png" width="55px"/><br />
                            <input type="radio" name="temps">
                            tortue
                        </label>
                    </div>
                </div>
            </div>
            <br />
            <div class="container">
                <div class="notification">
                    <div class="control has-text-centered">
                        <label class="radio">
                            <img src="img/icones/noun_1621405_cc.png" width="55px"/><br />
                            <input type="radio" name="foobar" checked>
                            1 à 8 heures
                        </label>
                        <label class="radio">
                            <img src="img/icones/noun_1621419_cc.png" width="55px"/><br />
                            <input type="radio" name="foobar">
                            1 à 3 jours
                        </label>
                        <label class="radio">
                            <img src="img/icones/noun_1621159_cc.png" width="55px"/><br />
                            <input type="radio" name="foobar">
                            1 semaine
                        </label>
                    </div>
                </div>
            </div>
            <br /> <br />
            <div class="column is-half is-offset-one-quarter has-text-centered">
                <a class="button is-danger">Valider</a>
            </div>
            

            <br /><br /><br /><br /><br /><br />
            <a id="application"></a>
            <section class="hero is-primary">
                <div class="hero-body">
                    <div class="container">
                    <h1 class="title">
                        Application
                    </h1>
                    <h2 class="subtitle">
                        Obtenez votre parcours
                    </h2>
                    </div>
                </div>
            </section>
            
            <br /><br /><br /><br /><br /><br />
            <a id="application"></a>
            <section class="hero is-light">
                <div class="hero-body">
                    <div class="container">
                    <h1 class="title">
                        Crédits
                    </h1>
                    <h2 class="subtitle">
                        Images et icônes
                    </h2>
                    </div>
                </div>
                <div class="container">
                    <div class="notification">
                        from the Noun Project<br />
                        <br />
                        Binoculars by Claire Jones<br />
                        Magnifier by Creative Stall<br />
                        Family by b farias from the Noun Project<br />
                        Tie by Daniel Nochta from the Noun Project<br />
                        Couple by b farias from the Noun Project<br />
                        Grandpa by b farias from the Noun Project<br />
                        Man by b farias from the Noun Project<br />
                        Handicap by Delwar Hossain from the Noun Project<br />
                        Dog by b farias from the Noun Project<br />
                        Woman pregnant by b farias from the Noun Project<br />
                        Male by Icons Bazaar from the Noun Project<br />
                        Female by Icons Bazaar from the Noun Project<br />
                        Run by Gregor Cresnar from the Noun Project<br />
                        Turtle by Alejandro Capellan from the Noun Project<br />
                        Backpack by Nook Fulloption from the Noun Project<br />
                        Arrow by iconcheese from the Noun Project
                    </div>
                </div>
            </section>
        </main>
        <footer>
            <br /><br />
            <div class="container">
                <div class="content has-text-centered">
                    <p>
                        <strong>HappyWalks</strong> by <a href="">Yann Darlet</a><br />
                        <a href="mailto:contact@happywalks.fr"><i class="fas fa-envelope"></i> contact@happywalks.fr</a>
                    </p>
                </div>
            </div>
            <br />
            <!--Copyright 2018.-->
        </footer>
    </body>
</html>