<a id="parcours"></a>
<section class="hero is-primary">
    <div class="hero-body">
        <div class="container">
        <h1 class="title">
            Parcours
        </h1>
        <h2 class="subtitle">
            Catalogue
        </h2>
        </div>
    </div>
</section>

<a id="parcour_all_in_one_day"></a>
<div class="box">
    <article class="media">
        <div class="media-left">
        <figure class="image is-128x128">
            <img src="img/parcours/all_in_one_day/all_in_one_day1.jpg" alt="Image">
        </figure>
        </div>
        <div class="media-content">
        <div class="content">
            <p>
            <strong>All in one day</strong> <small>#All_In_One_Day</small>
            <br>
            Faites le tour de Monaco, ne manquez aucun coin bucolique, découvrez les grands endroits qui font la renommée de la ville ainsi que les ruelles et les marchés célèbres. Tout en un jour va satisfaire votre soif de découvrir l'âme de la ville en vous faisant passer par plusieurs quartiers.
            </p>
        </div>
        <nav class="level is-mobile">
            <div class="level-left">
            <a class="level-item">
                <span class="icon is-small"><i class="fas fa-retweet"></i></span>
            </a>
            <a class="level-item">
                <span class="icon is-small"><i class="fas fa-heart"></i></span>
            </a>
            </div>
        </nav>
        </div>
    </article>
</div>

<a id="parcour_beausoleil"></a>
<div class="box">
    <article class="media">
        <div class="media-left">
        <figure class="image is-128x128">
            <img src="img/parcours/beausoleil/beausoleil3.jpg" alt="Image">
        </figure>
        </div>
        <div class="media-content">
        <div class="content">
            <p>
            <strong>Beausoleil</strong> <small>#Beausoleil</small>
            <br>
            Située sur le bassin versant qui surplombe la principauté de Monaco, la ville de Beausoleil est entourée de la « Tête de Chien » et du mont Agel. Bien que située en territoire français, la station ne forme avec Monte-Carlo qu'une seule agglomération, la commune étant limitrophe à la principauté de Monaco.
            </p>
        </div>
        <nav class="level is-mobile">
            <div class="level-left">
            <a class="level-item">
                <span class="icon is-small"><i class="fas fa-retweet"></i></span>
            </a>
            <a class="level-item">
                <span class="icon is-small"><i class="fas fa-heart"></i></span>
            </a>
            </div>
        </nav>
        </div>
    </article>
</div>

<a id="parcour_formule_1"></a>
<div class="box">
    <article class="media">
        <div class="media-left">
        <figure class="image is-128x128">
            <img src="img/parcours/formule1/formule1.jpg" alt="Image">
        </figure>
        </div>
        <div class="media-content">
        <div class="content">
            <p>
            <strong>Grand Prix</strong> <small>#Formule_1</small>
            <br>
            Le Grand Prix de Monaco est une des plus anciennes et l'une des trois courses les plus prestigieuses au monde, disputée en Principauté de Monaco, sur un circuit urbain. Nous vous proposons sa version interactive et personalisée !
            </p>
        </div>
        <nav class="level is-mobile">
            <div class="level-left">
            <a class="level-item">
                <span class="icon is-small"><i class="fas fa-retweet"></i></span>
            </a>
            <a class="level-item">
                <span class="icon is-small"><i class="fas fa-heart"></i></span>
            </a>
            </div>
        </nav>
        </div>
    </article>
</div>

<a id="parcour_vue_oiseau"></a>
<div class="box">
    <article class="media">
        <div class="media-left">
        <figure class="image is-128x128">
            <img src="img/parcours/a_vue_doiseau/jardin2.jpg" alt="Image">
        </figure>
        </div>
        <div class="media-content">
        <div class="content">
            <p>
            <strong>Vue d'oiseau</strong> <small>#Jardins_Monaco</small>
            <br>
            Découvrez le Monaco vert à travers les jardins, la roseraie et certains monuments incontournables. nous vous proposons d'aller sur les hauteurs pour admirer la vue imprenable depuis le jardin exotique, puis de redescendre au niveau du port.
            </p>
        </div>
        <nav class="level is-mobile">
            <div class="level-left">
            <a class="level-item">
                <span class="icon is-small"><i class="fas fa-retweet"></i></span>
            </a>
            <a class="level-item">
                <span class="icon is-small"><i class="fas fa-heart"></i></span>
            </a>
            </div>
        </nav>
        </div>
    </article>
</div>

<a id="parcour_oranger"></a>
<div class="box">
    <article class="media">
        <div class="media-left">
        <figure class="image is-128x128">
            <img src="img/parcours/oranger/oranger.jpeg" alt="Image">
        </figure>
        </div>
        <div class="media-content">
        <div class="content">
            <p>
            <strong>Rand'Oranger</strong> <small>#Orangers #Produits_locaux</small>
            <br>
            Entre mer et collines, en passant par une chocolaterie réputée dans tout le pays, des petits restaurants traditionnels, ou encore un bar brassant ses bières à quelques mètres de la mer, vous pourrez vous frayer un chemin sous nos célèbres orangers. En quelques mots, je vous invite à découvrir notre magnifique ville à travers les yeux de ma grand-mère ! »
            </p>
        </div>
        <nav class="level is-mobile">
            <div class="level-left">
            <a class="level-item">
                <span class="icon is-small"><i class="fas fa-retweet"></i></span>
            </a>
            <a class="level-item">
                <span class="icon is-small"><i class="fas fa-heart"></i></span>
            </a>
            </div>
        </nav>
        </div>
    </article>
</div>