<a id="preferences"></a>
<section class="hero is-info">
    <div class="hero-body">
        <div class="container">
        <h1 class="title">
            Préférences
        </h1>
        <h2 class="subtitle">
            Personalisation
        </h2>
        </div>
    </div>
</section>

<section class="hero">
    <div class="hero-body">
        <div class="container">
        <h1 class="title">
            Je suis
        </h1>
        <h2 class="subtitle">
            choisissez
        </h2>
        </div>
    </div>
</section>
<div class="container">
    <div class="notification">
        <div class="control has-text-centered">
            <label class="radio">
                <img src="img/icones/noun_395108_cc.png" width="80px"/><br />
                <input type="radio" name="type_visiteur1" checked>
                nouveau visiteur
            </label>
            <label class="radio">
                <img src="img/icones/noun_632074_cc.png" width="65px"/><br />
                <input type="radio" name="type_visiteur1">
                habitué(e)
            </label>
        </div>
    </div>
    <div class="notification">
        <div class="control has-text-centered">
            <label class="radio">
                <img src="img/icones/noun_1106963_cc.png" width="65px"/><br />
                <input type="radio" name="type_visiteur2" checked>
                famille
            </label>
            <label class="radio">
                <img src="img/icones/noun_349141_cc.png" width="60px"/><br />
                <input type="radio" name="type_visiteur2">
                business
            </label>
            <label class="radio">
                <img src="img/icones/noun_1106956_cc.png" width="65px"/><br />
                <input type="radio" name="type_visiteur2">
                couple
            </label>
            <label class="radio">
                <img src="img/icones/noun_1106953_cc.png" width="63px"/><br />
                <input type="radio" name="type_visiteur2">
                seul(e)
            </label>
            <label class="radio">
                <img src="img/icones/noun_1106972_cc.png" width="62px"/><br />
                <input type="radio" name="type_visiteur2">
                retraité(e)
            </label>
        </div>
    </div>
    <div class="notification">
        <div class="control has-text-centered">
            <label class="checkbox">
                <img src="img/icones/noun_1106955_cc.png" width="45px"/><br />
                <input type="checkbox">
                animaux
            </label>
            <label class="checkbox">
                <img src="img/icones/noun_1106982_cc.png" width="62px"/><br />
                <input type="checkbox">
                enceint
            </label>
            <label class="checkbox">
                <img src="img/icones/noun_1050600_cc.png" width="60px"/><br />
                <input type="checkbox">
                handicapé(e)
            </label>
            <label class="checkbox">
                <img src="img/icones/noun_640988_cc.png" width="60px"/><br />
                <input type="checkbox">
                équipé(e)
            </label>
        </div>
    </div>
</div>

<section class="hero">
    <div class="hero-body">
        <div class="container">
        <h1 class="title">
            J'aime
        </h1>
        <h2 class="subtitle">
            choisissez
        </h2>
        </div>
    </div>
</section>
<div class="container">
    <div class="notification">
        <div class="tags pointer-hand select-disabled">
            <span class="tag is-primary is-medium" onclick="onClickTag(this)">Nature</span>
            <span class="tag is-primary is-medium" onclick="onClickTag(this)">Océan</span>
            <span class="tag is-medium" onclick="onClickTag(this)">Casinos</span>
            <span class="tag is-medium" onclick="onClickTag(this)">Sport</span>
            <span class="tag is-medium" onclick="onClickTag(this)">Formule 1</span>
            <span class="tag is-medium" onclick="onClickTag(this)">Voitures</span>
            <span class="tag is-medium" onclick="onClickTag(this)">Gastronomie</span>
            <span class="tag is-primary is-medium" onclick="onClickTag(this)">Architecture</span>
            <span class="tag is-medium" onclick="onClickTag(this)">Histoire</span>
            <span class="tag is-primary is-medium" onclick="onClickTag(this)">Plantes</span>
            <span class="tag is-primary is-medium" onclick="onClickTag(this)">Marcher</span>
            <span class="tag is-medium" onclick="onClickTag(this)">Musées</span>
            <span class="tag is-medium" onclick="onClickTag(this)">Animaux</span>
            <span class="tag is-medium" onclick="onClickTag(this)">Bâteaux</span>
            <span class="tag is-medium" onclick="onClickTag(this)">Peinture</span>
            <span class="tag is-primary is-medium" onclick="onClickTag(this)">Musique</span>
            <span class="tag is-medium" onclick="onClickTag(this)">Techologies</span>
            <span class="tag is-medium" onclick="onClickTag(this)">Jeux-Vidéos</span>
            <span class="tag is-medium" onclick="onClickTag(this)">Produits Locaux</span>
            <span class="tag is-medium" onclick="onClickTag(this)">Timbres</span>
        </div>
    </div>
</div>
<section class="hero">
    <div class="hero-body">
        <div class="container">
        <h1 class="title">
            J'ai le temps
        </h1>
        <h2 class="subtitle">
            choisissez
        </h2>
        </div>
    </div>
</section>
<div class="container">
    <div class="notification">
        <div class="control has-text-centered">
            <label class="radio">
                <img src="img/icones/noun_1428994_cc.png" width="50px"/><br />
                <input type="radio" name="temps" checked>
                pressé
            </label>
            <label class="radio">
                <img src="img/icones/noun_709031.png" width="58px"/><br />
                <input type="radio" name="temps">
                modéré
            </label>
            <label class="radio">
                <img src="img/icones/noun_181032_cc.png" width="55px"/><br />
                <input type="radio" name="temps">
                tortue
            </label>
        </div>
    </div>
</div>
<br />
<div class="container">
    <div class="notification">
        <div class="control has-text-centered">
            <label class="radio">
                <img src="img/icones/noun_1621405_cc.png" width="55px"/><br />
                <input type="radio" name="foobar" checked>
                1 à 8 heures
            </label>
            <label class="radio">
                <img src="img/icones/noun_1621419_cc.png" width="55px"/><br />
                <input type="radio" name="foobar">
                1 à 3 jours
            </label>
            <label class="radio">
                <img src="img/icones/noun_1621159_cc.png" width="55px"/><br />
                <input type="radio" name="foobar">
                1 semaine
            </label>
        </div>
    </div>
</div>
<br /> <br />
<div class="column is-half is-offset-one-quarter has-text-centered">
    <a class="button is-danger">Valider</a>
</div>