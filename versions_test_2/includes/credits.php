<a id="credits"></a>
<section class="hero is-light">
    <div class="hero-body">
        <div class="container">
        <h1 class="title">
            Crédits
        </h1>
        <h2 class="subtitle">
            Images et icônes
        </h2>
        </div>
    </div>
    <div class="container">
        <div class="notification">
            from the Noun Project<br />
            <br />
            Binoculars by Claire Jones<br />
            Magnifier by Creative Stall<br />
            Family by b farias from the Noun Project<br />
            Tie by Daniel Nochta from the Noun Project<br />
            Couple by b farias from the Noun Project<br />
            Grandpa by b farias from the Noun Project<br />
            Man by b farias from the Noun Project<br />
            Handicap by Delwar Hossain from the Noun Project<br />
            Dog by b farias from the Noun Project<br />
            Woman pregnant by b farias from the Noun Project<br />
            Male by Icons Bazaar from the Noun Project<br />
            Female by Icons Bazaar from the Noun Project<br />
            Run by Gregor Cresnar from the Noun Project<br />
            Turtle by Alejandro Capellan from the Noun Project<br />
            Backpack by Nook Fulloption from the Noun Project<br />
            Arrow by iconcheese from the Noun Project
        </div>
    </div>
</section>