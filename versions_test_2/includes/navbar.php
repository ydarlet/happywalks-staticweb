<div class="navbar-brand">
    <a class="navbar-item" href="">
        <img src="img/logo-hw-2-yellow-crop.png" alt="Happywalks: application de parcours personalisés" height="28">
    </a>
    <div class="navbar-burger burger" data-target="navbarExampleTransparentExample">
    <span>Home</span>
    <span>Parcours</span>
    <span>Préférences</span>
    <span>Application</span>
    </div>
</div>

<div id="navbarExampleTransparentExample" class="navbar-menu">
    <div class="navbar-start">
    <a class="navbar-item" href="">
        Accueil
    </a>
    <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link" href="#parcours">
        Parcours
        </a>
        <div class="navbar-dropdown is-boxed">
        <a class="navbar-item" href="#parcours">
            Tous
        </a>
        <hr class="navbar-divider">
        <a class="navbar-item" href="#parcour_all_in_one_day">
            All in One Day
        </a>
        <a class="navbar-item" href="#parcour_beausoleil">
            Beausoleil
        </a>
        <a class="navbar-item" href="#parcour_formule_1">
            Formule 1
        </a>
        <a class="navbar-item" href="#parcour_vue_oiseau">
            Vue d'oiseau
        </a>
        <a class="navbar-item" href="#parcour_vue_oiseau">
            Rand'Oranger
        </a>
        
        </div>
    </div>
    <a class="navbar-item" href="#preferences">
        Préférences
    </a>
    <a class="navbar-item" href="#application">
        Application
    </a>
    </div>

    <div class="navbar-end">
    <div class="navbar-item">
        <div class="field is-grouped">
        <p class="control">
            <a class="bd-tw-button button" data-social-network="Twitter" data-social-action="tweet" data-social-target="http://localhost:4000" target="_blank" href="https://twitter.com/intent/tweet?text=Bulma: a modern CSS framework based on Flexbox&amp;hashtags=bulmaio&amp;url=http://localhost:4000&amp;via=jgthms">
            <span class="icon">
                <i class="fab fa-twitter"></i>
            </span>
            <span>
                Tweet
            </span>
            </a>
        </p>
        <p class="control">
            <a class="button is-link" href="https://github.com/jgthms/bulma/archive/0.5.1.zip">
            <span class="icon">
                <i class="fas fa-download"></i>
            </span>
            <span>Download</span>
            </a>
        </p>
        </div>
    </div>
    </div>
</div>