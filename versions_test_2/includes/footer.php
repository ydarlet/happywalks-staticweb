<br /><br />
<div class="container">
    <div class="content has-text-centered">
        <p>
            <strong>HappyWalks</strong> by <a href="">Yann Darlet</a><br />
            <a href="mailto:contact@happywalks.fr"><i class="fas fa-envelope"></i> contact@happywalks.fr</a>
        </p>
    </div>
</div>
<br />
<!--Copyright 2018.-->