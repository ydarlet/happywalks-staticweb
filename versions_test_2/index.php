<!doctype html>
<html lang="fr">

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <title>Happywalks.fr ~ accueil</title>
        <link rel="icon" type="image/png" href="img/logo-hw-2-yellow-crop.png" />
        <link rel="stylesheet" href="css/happywalks.css">
    </head>

    <body class="yellow-back1 ubuntu" style="height: 100%;">

        <main style="text-align:center; margin-top: 30vh;">
            <figure>
                <a href="home.php">
                    <img    
                        src="img/logo-hw-2-yellow-crop.png"
                        alt="happywalks-logo" title="Enjoy you trip !" 
                        width="128px" 
                    />
                </a>
            </figure>
            <h1 class="title shadow1">Happy Walks</h1>
        </main>

    </body>
    
</html>