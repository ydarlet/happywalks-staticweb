<!doctype html>
<html lang="fr">

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <title>Happywalks.fr ~ catalogue</title>
        <link rel="icon" type="image/png" href="../img/logo-hw-2-yellow-crop.png" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css">
        <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
        <script src="../js/happywalks.js"></script>
        <link rel="stylesheet" href="../css/happywalks.css">
    </head>

    <body class="" style="min-height: 114vh;">

        <header>  
        </header>

        <main>
            <h1 class="title" style="color: white; position: absolute; left: 17.5vw; top: 1vh;">
                <a href="../catalogue.php" style="color:white;">
                    <i class="fas fa-angle-left"></i>
                </a>
            </h1>
            <center>
                <figure>
                    <img src="../img/parcours/all_in_one_day/all_in_one_day_ratio.png" style="min-height:50vh; max-height:70vh;margin-bottom: -1vh;"/>
                </figure>
            </center>
            <section class="hero is-dark" style="max-width: 847px; margin: auto;">
                <div class="hero-body">
                    <div class="container">
                        <h1 class="title">
                            All in one day
                        </h1>
                        <h2 class="subtitle">
                            Une journée bien remplie
                        </h2>
                    </div>
                </div>
            </section>
            <section class="hero is-light" style="max-width: 847px; margin: auto;">
                <div class="hero-body">
                    <div class="container">
                        <h1 class="title is-4">
                            14 Étapes - 2h - 14 km
                        </h1>
                        <h2 class="subtitle is-6">
                            #Musée #Océan #Nature #Architecture #Jardins  [...]
                        </h2>
                    </div>
                </div>
                <div class="hero-body">
                    <div class="container">
                        <h1 class="title is-4">
                            Etape 1
                        </h1>
                        <h2 class="subtitle is-4">
                            Musée Océanographique
                        </h2>
                        <p style="max-width: 740px;">
                        Date de création : fondé en 1889 par le Prince Albert Ier de Monaco et inauguré en 1910
                        <br />
                        Petite histoire : musée de la mer, basé sur le Rocher de Monaco sur la Côte d'Azur. Il est constitué de 90 bassins et sa collection est riche de 350 espèces de poissons.
                        <br />
                        Temps de la visite :  1h 30 - 2h
                        </p>
                    </div>
                </div>
                <div class="hero-body">
                    <div class="container">
                        <h1 class="title is-4">
                            Etape 2
                        </h1>
                        <h2 class="subtitle is-4">
                            Jardin Saint Martin
                        </h2>
                        <p style="max-width: 740px;">
                        Petite histoire : Situé dans le quartier Monaco-Ville. Belle endroit pour se promener, avec une grande diversité florale. Des fortifications médiévales, plusieurs terrasses avec la belle vue sur la mer. Statue du Prince Albert Ier en tant que marin.
                        <br />
                        Temps de la visite : 15 min ou 2h
                        </p>
                    </div>
                </div>
                <div class="hero-body">
                    <div class="container">
                        <h1 class="title is-4">
                            Etape 3
                        </h1>
                        <h2 class="subtitle is-4">
                            Cathédrale de Monaco
                        </h2>
                        <p style="max-width: 740px;">
                        Date de création: 1875 - 1903 <br />
                        Petite histoire: Remplace l'église Saint- Nicolas. Placé sur le rocher de Monaco avec un Architecture romano-byzantin. Symbole de la foi chrétienne de la principauté. On y trouve Les tombes de la plupart des Princes, dont Rainier III et son épouse la Princesse Grace. Quelques trésors artistiques 
                        <br />Temps de la visite: 30 min
                        </p>
                    </div>
                </div>
            </section>
        </main>

        <footer>
            <div style="text-align: center; position: fixed; bottom: 0px; height: 7vh; width: 100vw; background-color: #ccc; padding: 1vh;">
                Naviguer - Partager - Réalité Augmentée - Plan
            </div>
        </footer>

    </body>
    
</html>