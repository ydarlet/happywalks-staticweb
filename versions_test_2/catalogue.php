<!doctype html>
<html lang="fr">

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <title>Happywalks.fr ~ catalogue</title>
        <link rel="icon" type="image/png" href="img/logo-hw-2-yellow-crop.png" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css">
        <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
        <script src="js/happywalks.js"></script>
        <link rel="stylesheet" href="css/happywalks.css">
    </head>

    <body class="ubuntu">

        <header>  
        </header>

        <main>
            <section class="hero is-light">
                <div class="hero-body">
                    <div class="container">
                        <h1 class="title">
                            Parcours Disponibles
                        </h1>
                        <h2 class="subtitle">
                            Principauté de Monaco
                        </h2>
                    </div>
                </div>
            </section>
            <section class="section is-paddingless" style="display: block; position: relative; min-height: 100vh;">
                <div class="tile is-ancestor">
                    <div class="tile is-vertical">
                        <div class="tile">
                            <div class="tile is-parent is-vertical">
                                <article class="tile is-child notification is-warning is-paddingless">
                                    <div class="text-catalogue">
                                        <a href="parcours/monaco-all-in-one-day.php" style="text-decoration:none;">
                                            <h1 class="title is-4">All in One Day</h1>
                                            <h2 class="subtitle is-6"> 2h - 14km</h2>
                                            <div class="catalogue-3dots">
                                                <i class="fas fa-ellipsis-h"></i>
                                            </div>
                                        </a>
                                    </div>
                                    <center>
                                        <a href="parcours/monaco-all-in-one-day.php">
                                            <figure class="image is-5by3 image-catalogue">
                                                <img src="img/parcours/all_in_one_day/all_in_one_day_ratio.png"/>
                                            </figure>
                                        </a>
                                    </center>
                                </article>
                                <article class="tile is-child notification is-warning is-paddingless">
                                    <div class="text-catalogue">
                                        <a href="catalogue.php" style="text-decoration:none;">
                                            <h1 class="title is-4">Vol d'Oiseau</h1>
                                            <h2 class="subtitle is-6"> 4h - 26km</h2>
                                            <div class="catalogue-3dots">
                                                <i class="fas fa-ellipsis-h"></i>
                                            </div>
                                        </a>
                                    </div>
                                    <center>
                                        <a href="catalogue.php">
                                            <figure class="image is-5by3 image-catalogue">
                                                <img src="img/parcours/a_vue_doiseau/jardin_800_480_ratio.png"/>
                                            </figure>
                                        </a>
                                    </center>
                                </article>
                            </div>
                            <div class="tile is-parent is-vertical">
                                <article class="tile is-child notification is-warning is-paddingless">
                                    <div class="text-catalogue">
                                        <a href="catalogue.php" style="text-decoration:none;">
                                            <h1 class="title is-4">Grand Prix</h1>
                                            <h2 class="subtitle is-6">1h30 - 2km</h2>
                                            <div class="catalogue-3dots">
                                                <i class="fas fa-ellipsis-h"></i>
                                            </div>
                                        </a>
                                    </div>
                                    <center>
                                        <a href="catalogue.php">
                                            <figure class="image is-5by3 image-catalogue">
                                                <img src="img/parcours/formule1/formule1_ratio.png"/>
                                            </figure>
                                        </a>
                                    </center>
                                </article>
                                <article class="tile is-child notification is-warning is-paddingless">
                                    <div class="text-catalogue">
                                        <a href="catalogue.php" style="text-decoration:none;">
                                            <h1 class="title is-4">Beausoleil</h1>
                                            <h2 class="subtitle is-6">2h30 - 10km</h2>
                                            <div class="catalogue-3dots">
                                                <i class="fas fa-ellipsis-h"></i>
                                            </div>
                                        </a>
                                    </div>
                                    <center>
                                        <a href="catalogue.php">
                                            <figure class="image is-5by3 image-catalogue">
                                                <img src="img/parcours/beausoleil/beausoleil1_ratio.png"/>
                                            </figure>
                                        </a>
                                    </center>
                                </article>
                            </div>
                            
                        </div>
                        <div class="tile is-parent is-6">
                                <article class="tile is-child notification is-warning is-paddingless">
                                    <div class="text-catalogue">
                                        <a href="catalogue.php" style="text-decoration:none;">
                                            <h1 class="title is-4">Rand'Oranger</h1>
                                            <h2 class="subtitle is-6">1h - 2km</h2>
                                            <div class="catalogue-3dots">
                                                <i class="fas fa-ellipsis-h"></i>
                                            </div>
                                        </a>
                                    </div>
                                    <center>
                                        <a href="catalogue.php">
                                            <figure class="image is-5by3 image-catalogue">
                                                <img src="img/parcours/oranger/oranger1_ratio2.png"/>
                                            </figure>
                                        </a>
                                    </center>
                                </article>
                        </div>
                    </div>
                </div>
            </section>
        </main>

        <footer>
        </footer>

    </body>
    
</html>