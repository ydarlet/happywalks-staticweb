<!doctype html>
<html lang="fr">

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <title>Happywalks.fr ~ accueil</title>
        <link rel="icon" type="image/png" href="img/logo-hw-2-yellow-crop.png" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css">
        <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
        <script src="js/happywalks.js"></script>
        <link rel="stylesheet" href="css/happywalks.css">
    </head>

    <body class="yellow-back1 ubuntu" style="height: 100vh; overflow:hidden;">

        <header>  
        </header>

        <main>
            <section class="section">
                <div class="container">
                    <div class="tile is-ancestor">
                        <div class="tile is-vertical">
                            <div class="tile">
                                <div class="tile is-parent is-vertical">
                                    <article class="tile is-child notification is-primary radius" style="min-height: 43vh;">
                                        <center>
                                            <a href="catalogue.php">
                                                <figure class="image is-128x128" style="margin-top: 5vh;">
                                                    <img src="img/logo-hw-2-yellow-crop.png" alt="happywalks-logo" title="Enjoy you trip !" width="120px" />
                                                </figure>
                                            </a>
                                            <br />
                                            <h1 class="title" style="color: ivory">Guest</h1>
                                        </center>
                                    </article>
                                    <article class="tile is-child notification is-link radius" style="min-height: 43vh;">
                                        <center>
                                            <figure class="image is-128x128" style="margin-top: 5vh;">
                                                <img src="img/logo-hw-2-yellow-crop.png" alt="happywalks-logo" title="Enjoy you trip !" width="120px" />
                                            </figure>
                                            <br />
                                            <h1 class="title" style="color: ivory">Login</h1>
                                        </center>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>

        <footer>
        </footer>

    </body>
    
</html>